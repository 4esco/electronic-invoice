<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {

	if ($mode == 'update') {

                if (isset($_POST['staff_notes']) && $_POST['staff_notes'] != $_POST['old_staff_notes']) {
                    
                    fn_request_electronic_invoice_update_staff_notes($_POST['electronic_invoice_id'], $_POST['staff_notes']);
                }

		$file_types = array(
		    'upload_pdf_file',
		    'upload_xml_file'
		);
		$id = $_SESSION['request_history']['current']['params']['request_id'];
		
		foreach ($file_types as $file_type) {
		    $attachment_id = db_get_field("SELECT attachment_id FROM ?:request_attachments WHERE object_id = ?i AND file_type = ?s", $id, $file_type);
		    $data = array(
			'file_type' => $file_type
		    );
		    if ($file_type == 'upload_pdf_file') {	    
		    $filter = fn_filter_uploaded_data($file_type, array('pdf'));
		    }
		    
		    if ($file_type == 'upload_xml_file') {	    
		    $filter = fn_filter_uploaded_data($file_type, array('xml'));
		    }	    
		    fn_request_electronic_invoice_update_attachments($data, $attachment_id, 'C', $id, 'M', null, $file_type, $filter);
		}
		$suffix = ".details?request_id=$id";
	}

	if ($mode == 'delete_attachment') {
		fn_request_electronic_invoice_delete_attachments($_REQUEST['attachment_id']);
		if (defined('AJAX_REQUEST')) {
		    Tygh::$app['ajax']->assign('deleted', true);
		}
		exit;
	}

		return array(CONTROLLER_STATUS_OK, 'electronic_invoice' . $suffix);
	}

if ($mode == 'manage') { 
    
    list($electronic_invoice, $search) = fn_request_electronic_invoice($_REQUEST, Registry::get('settings.Appearance.admin_elements_per_page'));

    Tygh::$app['view']->assign('orders', $electronic_invoice);
    Tygh::$app['view']->assign('search', $search);

}

if ($mode == 'get_file') {
    if (!empty($_REQUEST['attachment_id'])) {
        fn_request_electronic_invoice_get_attachment($_REQUEST['attachment_id']);
    }
    exit;
}

if ($mode == 'details') { 

    if (!empty($_REQUEST['request_id'])) {

	$data = db_get_array("SELECT * FROM ?:electronic_invoice AS el_invoice LEFT JOIN ?:country_descriptions AS country_descr ON el_invoice.country = country_descr.code LEFT JOIN ?:payment_descriptions AS pay_descr ON el_invoice.payment_id = pay_descr.payment_id WHERE country_descr.lang_code =?s AND pay_descr.lang_code =?s AND el_invoice.electronic_invoice_id =?i", CART_LANGUAGE, CART_LANGUAGE, $_REQUEST['request_id'] );
		
	if (!empty($data)) {
		
            $state_descr = db_get_field("SELECT state_descr.state FROM ?:states AS state LEFT JOIN ?:state_descriptions AS state_descr ON state.state_id = state_descr.state_id WHERE state.country_code=?s AND state.code=?s", $data['0']['code'], $data['0']['state']);
            
            Tygh::$app['view']->assign('state_descr', $state_descr);
    	}
		
	$attachment = db_get_hash_array("SELECT *  FROM ?:request_attachments WHERE object_id=?i",'file_type', $_REQUEST['request_id']);
		
	foreach ($data as $data){
	}
	
        Tygh::$app['view']->assign('data', $data);
	Tygh::$app['view']->assign('attachment', $attachment);
    }
}