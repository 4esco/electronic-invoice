<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'index') {
    
    if ($auth['user_type'] == 'V') {
  
        $logs = Tygh::$app['view']->getTemplateVars('logs');
        
        if ($logs) {

            foreach ($logs as $k => $v) {

                if ($v['type'] == 'ei') {
                    
                    $notification = db_get_field('SELECT notification FROM ?:electronic_invoice WHERE electronic_invoice_id = ?i', $v['content']['id']);
                    
                    if ($notification != 0) {
                        
                        switch ($notification) {
                            
                            case 1: fn_set_notification('W', __('information') , __('new_ei')); break;
                            case 2: fn_set_notification('W', __('information') , __('the_eir') . " #" . $v['content']['id'] . " " . __('eir_reopened')); break;
                        }
                        
                        db_query('UPDATE ?:electronic_invoice SET notification = ?i WHERE electronic_invoice_id = ?i', 0, $v['content']['id']);
                    }
                }
            }

            unset($k);
            unset($v);
        }
    }
}