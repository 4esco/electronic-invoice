<?php

use Tygh\Registry;
use Tygh\Storage;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'request') {
        
        $rei_data = $_REQUEST['rei_data'];
        
        if ($rei_data) {
            
            $rei_data['payment_id'] = db_get_field('SELECT payment_id FROM ?:orders WHERE order_id = ?i', $_POST['order_id']);
            $rei_data['company_id'] = db_get_field('SELECT company_id FROM ?:orders WHERE order_id = ?i', $_POST['order_id']);
            $rei_data['link'] = $_POST['order_id'];
            $rei_data['timestamp'] = TIME;

            $electronic_invoice_id = db_query('INSERT INTO ?:electronic_invoice ?e', $rei_data);

            $content = array (
                'ei' => 'Electronic invoice (#' . $electronic_invoice_id . ')',
                'id' => $electronic_invoice_id,
            );
            
            $content = serialize($content);
            
            $row = array (
                'user_id' => $auth['user_id'],
                'timestamp' => TIME,
                'type' => 'ei',
                'action' => 'request',
                'event_type' => 'N',
                'content' => $content,
                'backtrace' => '',
                'company_id' => $rei_data['company_id'],
            );

            db_query("INSERT INTO ?:logs ?e", $row);
            
            db_query('UPDATE ?:orders SET can_rei = ?i WHERE order_id = ?i', 0, $_POST['order_id']);
            
            fn_set_notification('N', __('notice') , __('ei_was_sent'));
        }
        else {
            
            fn_set_notification('E', __('error'), __('error_occurred'));
        }
    }

    return array(CONTROLLER_STATUS_OK, 'orders.details&order_id=' . $_POST['order_id']);
}

if ($mode == 'request') {

    fn_add_breadcrumb(__('request_electronic_invoice'));
    
    Tygh::$app['view']->assign('countries', fn_get_simple_countries(false, CART_LANGUAGE));
    Tygh::$app['view']->assign('states', fn_get_all_states());
}

if ($mode == 'get_file') {

    if (!empty($_REQUEST['attachment_id'])) {
 
        fn_request_electronic_invoice_get_attachment($_REQUEST['attachment_id']);
    }
    
    exit;
}