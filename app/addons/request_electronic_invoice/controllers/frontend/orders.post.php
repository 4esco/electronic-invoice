<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'details') {
    
    $ei_ids = db_get_fields('SELECT electronic_invoice_id FROM ?:electronic_invoice WHERE link = ?i AND status = ?s', $_REQUEST['order_id'], 'S');

    $ei_attachments = db_get_array("SELECT * FROM ?:request_attachments WHERE object_id IN (?a)", $ei_ids);

    if (count($ei_attachments) > 0) {
            
        Tygh::$app['view']->assign('attachments_data', $ei_attachments);
           
        $navigation_tabs = Registry::get('navigation.tabs');

        $navigation_tabs['ei_attachments'] = array(
            'title' => __('ei'),
            'js' => true,
            'href' => 'orders.details?order_id=' . $_REQUEST['order_id'] . '&selected_section=ei_attachments'
        );
        Registry::set('navigation.tabs', $navigation_tabs);
    }
}