<?php
 
use Tygh\Registry;
use Tygh\Storage;
use Tygh\Mailer;
 
if (!defined('BOOTSTRAP')) { die('Access denied'); }
 
function fn_request_electronic_invoice_get_order_items_info_post($order) {

    $days_to_rei = Registry::get('addons.request_electronic_invoice.days_to_rei');
    $days = date_diff(date_create(date("Y-m-d",$order['timestamp'])), date_create(date("Y-m-d")))->days;

    Tygh::$app['view']->assign('can_rei', ($order['can_rei'] == 1 && $days_to_rei >= $days && !in_array($order['status'], array('Y', 'O', 'F', 'D', 'I'))) ? 1 : '');
}

function fn_request_electronic_invoice_update_staff_notes($electronic_invoice_id, $staff_notes) {

    if (!empty($electronic_invoice_id)) {
        
        if (!empty($staff_notes)) {
        
            db_query("UPDATE ?:electronic_invoice SET staff_notes = ?s, staff_notes_date = ?i, status = ?s, notification = ?i WHERE electronic_invoice_id = ?i", $_POST['staff_notes'], time(), 'O', 2, $_POST['electronic_invoice_id']);
        }
        
        else {
            
            db_query("UPDATE ?:electronic_invoice SET staff_notes = ?s, staff_notes_date = ?i WHERE electronic_invoice_id = ?i", $_POST['staff_notes'], time(), $_POST['electronic_invoice_id']);
        }
        
        return true;
    }
}

function fn_request_electronic_invoice($params, $items_per_page = 10) {

	$default_params = array (
		'page' => 1,
		'items_per_page' => $items_per_page,
                'status_open' => 'Y',
                'status_sent' => 'Y',
	);

	$params = array_merge($default_params, $params);
	$limit = ''; 
	$sortings = array(
		'id' => "electronic_invoice_id",
		'name' => "name",
		'type' => "type",
		'status' => "status",
                'link' => "link",
                'timestamp' => "timestamp",
	);
	
	$sorting = db_sort($params, $sortings, 'timestamp', 'desc');

        $condition = '';

        if (isset($params['electronic_invoice_id']) && $params['electronic_invoice_id'] != '') {
            $condition .= db_quote(' AND electronic_invoice_id = ?i ', $params['electronic_invoice_id']);
        }
        
        if (isset($params['link']) && $params['link'] != '') {
            $condition .= db_quote(' AND link = ?i', $params['link']);
        }
        
        if (!empty($params['name_or_company'])) {
            $condition .= db_quote(" AND name LIKE ?l", "%" . trim($params['name_or_company']) . "%");
        }
        
        if (!empty($params['type'])) {
            $condition .= db_quote(' AND type = ?s', $params['type']);
        }
        
        $status = array();

        if ($params['status_open'] == 'Y') {
            array_push($status, 'O');
        }
        
        if ($params['status_sent'] == 'Y') {
            array_push($status, 'S');
        }
        
        $condition .= db_quote(' AND status IN (?a)', $status);
        
        if (!empty($params['date_from'])) {
            $condition .= db_quote(' AND timestamp >= ?i', fn_parse_date($params['date_from']));
        }

        if (!empty($params['date_to'])) {
            $condition .= db_quote(' AND timestamp <= ?i', fn_parse_date($params['date_to'], true));
        }

	if (!empty($params['items_per_page'])) {
            
            if (empty($_SESSION['auth']['company_id'])) {
                
		$params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:electronic_invoice WHERE 1 $condition");
            }
            else {
                
                $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:electronic_invoice WHERE company_id = ?i $condition", $_SESSION['auth']['company_id']);
            }
            
            $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
	}

	if (!empty($_SESSION['auth']['company_id'])) {
		$main_data = db_get_array("SELECT * FROM ?:electronic_invoice WHERE company_id =?i $condition $sorting $limit", $_SESSION['auth']['company_id']);
	} else {
		$main_data = db_get_array("SELECT * FROM ?:electronic_invoice WHERE 1 $condition $sorting $limit");
	}

	return array ($main_data, $params);
}

function fn_request_electronic_invoice_update_attachments($attachment_data, $attachment_id, $object_type, $object_id, $type = 'M', $files = null, $name, $filter)
{

if (!empty($filter)) {
   $object_id = intval($object_id);
   $directory = $object_type . '/' . $object_id;

    if ($files != null) {
        $uploaded_data = $files;
    } else {
        $uploaded_data = fn_filter_uploaded_data($name);
    }
    if (!empty($attachment_id)) {

        $rec = array (
            'position' => '0',
            'file_type' => $attachment_data['file_type'],
            'timestamp' => time()
        );

        $key = key($uploaded_data);
        if (!empty($uploaded_data[$key])) {
            $uploaded_data[$attachment_id] = $uploaded_data[$key];
        }

        db_query("UPDATE ?:request_attachments SET ?u WHERE attachment_id = ?i AND object_type = ?s AND object_id = ?i AND type = ?s", $rec, $attachment_id, $object_type, $object_id, $type);

    } elseif (!empty($uploaded_data)) {
        $rec = array (
            'object_type' => $object_type,
            'object_id' => $object_id,
            'position' => '0',
            'file_type' => $attachment_data['file_type'],
            'timestamp' => time()
        );
        if ($type !== null) {
            $rec['type'] = $type;
        } elseif (!empty($attachment_data['type'])) {
            $rec['type'] = $attachment_data['type'];
        }

        $attachment_id = db_query("INSERT INTO ?:request_attachments ?e", $rec);

        if ($attachment_id) {
            $key = key($uploaded_data);
            $uploaded_data[$attachment_id] = $uploaded_data[$key];
            unset($uploaded_data[1]);
       }

   }
}
    if ($attachment_id && !empty($uploaded_data[$attachment_id]) && $uploaded_data[$attachment_id]['size']) {
        $filename = $uploaded_data[$attachment_id]['name'];

        $old_filename = db_get_field("SELECT filename FROM ?:request_attachments WHERE attachment_id = ?i", $attachment_id);
        
        if ($old_filename) {
            Storage::instance('request_attachments')->delete($directory . '/' . $old_filename);
        }

        list($filesize, $filename) = Storage::instance('request_attachments')->put($directory . '/' . $filename, array(
            'file' => $uploaded_data[$attachment_id]['path']
        ));

        if ($filesize) {
            $filename = fn_basename($filename);
            db_query("UPDATE ?:request_attachments SET filename = ?s, filesize = ?i WHERE attachment_id = ?i", $filename, $filesize, $attachment_id);
        }
        
        if (db_get_field('SELECT status FROM ?:electronic_invoice WHERE electronic_invoice_id = ?i', $object_id) == 'O') {
        
            fn_request_electronic_invoice_update_status($_POST['electronic_invoice_id']);
        }
    }
    return $attachment_id;
}

function fn_request_electronic_invoice_update_status($object_id) {
    
    db_query("UPDATE ?:electronic_invoice SET status = ?s WHERE electronic_invoice_id =?i", 'S', $object_id);
    
    $mail_info = db_get_array("SELECT * FROM ?:electronic_invoice AS el_invoice LEFT JOIN ?:orders AS orders ON el_invoice.link = orders.order_id WHERE electronic_invoice_id =?i", $object_id);

    foreach ($mail_info as $mail_data) {
    }

    Mailer::sendMail(array( // Send email
        'to' => $mail_data['email'],
        'from' => 'company_orders_department',
        'data' => array(
            'order_info' => $mail_data,
            ),
        'tpl' => 'addons/request_electronic_invoice/orders/order_mail_notification.tpl',
        'company_id' => $mail_data['company_id'],
    ), 'C', $mail_data['lang_code']);
    
    return true;
}

function fn_request_electronic_invoice_get_attachment($attachment_id)
{

    $data = db_get_row("SELECT * FROM ?:request_attachments WHERE attachment_id = ?i", $attachment_id);
    Storage::instance('request_attachments')->get($data['object_type'] . '/' . $data['object_id'] . '/' . $data['filename']);

    return true;
}

function fn_request_electronic_invoice_delete_attachments($company_id)
{
    $data = db_get_hash_array("SELECT * FROM ?:request_attachments WHERE attachment_id = ?i", 'attachment_id', $company_id);
    
    if (!empty($data) && is_array($data)) {
        foreach($data as $attachment_id => $attachment_data) {
            $directory = $attachment_data['object_type'] . '/' . $attachment_data['object_id'];
            Storage::instance('request_attachments')->delete($directory . '/' . $attachment_data['filename']);
            db_query("DELETE FROM ?:request_attachments WHERE attachment_id = ?i", $attachment_id);
        }
    }

   return true;
 }