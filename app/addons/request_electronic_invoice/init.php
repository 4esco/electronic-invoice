<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'get_order_items_info_post'
);

Registry::set('config.storage.request_attachments', array(
    'prefix' => 'request_attachments',
    'secured' => true,
    'dir' => Registry::get('config.dir.var')
));