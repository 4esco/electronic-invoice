<div class="sidebar-row">
    <h6>{__("search")}</h6>

<form action="{""|fn_url}" name="electronic_invoice_search_form" method="get">

{if $smarty.request.redirect_url}
    <input type="hidden" name="redirect_url" value="{$smarty.request.redirect_url}" />
{/if}
{if $selected_section != ""}
    <input type="hidden" id="selected_section" name="selected_section" value="{$selected_section}" />
{/if}

{$extra nofilter}

{capture name='simple_search'}
<div class="sidebar-field">
    <label for="electronic_invoice_id">{__("id_of_the_request")}:</label>
    <input type="text" name="electronic_invoice_id" id="electronic_invoice_id" value="{$search.electronic_invoice_id}" size="30"/>
</div>

<div class="sidebar-field">
    <label for="link">{__("order_id")}:</label>
    <input type="text" name="link" id="link" value="{$search.link}" size="15"/>
</div>

<div class="sidebar-field">
    <label for="name_or_company">{__("name_or_company")}:</label>
    <input type="text" name="name_or_company" id="name_or_company" value="{$search.name_or_company}" size="15"/>
</div>

<div class="sidebar-field">
    <label for="type">{__("rei_type")}:</label>
        <select class="span3" name="type" id="type">
            <option value="" {if $search.type == ""}selected="selected"{/if}></option>
            <option value="P" {if $search.type == "P"}selected="selected"{/if}>{__("person")}</option>
            <option value="C" {if $search.type == "C"}selected="selected"{/if}>{__("company")}</option>
        </select>
</div>    

<div class="sidebar-field">
    <label>{__("status")}:</label>

    <label for="status_open" class="checkbox inline">
        <input type="hidden" value="N" name="status_open"/>
        <input type="checkbox" class="cm-toggle-checkbox" value="Y" name="status_open" id="status_open" {if $search.status_open == "Y"} checked="checked"{/if} />
        {__("open")}   
    </label>
    
    <label for="status_sent" class="checkbox inline">
        <input type="hidden" value="N" name="status_sent"/>
        <input type="checkbox" class="cm-toggle-checkbox" value="Y" name="status_sent" id="status_sent" {if $search.status_sent == "Y"} checked="checked"{/if} />
        {__("sent")}
    </label>
</div>            
    
<div class="sidebar-field" style="margin-top: 10px">    
    <label>{__("select_dates")}:</label>
    
    {include file="common/calendar.tpl" date_id="f_date" date_name="date_from" date_val=$search.date_from  start_year=$settings.Company.company_start_year date_meta=$date_meta}
    &nbsp;-&nbsp;
    {include file="common/calendar.tpl" date_id="t_date" date_name="date_to" date_val=$search.date_to  start_year=$settings.Company.company_start_year date_meta=$date_meta}
</div>

{/capture}



{include file="common/advanced_search.tpl" no_adv_link=true simple_search=$smarty.capture.simple_search dispatch=$dispatch not_saved=true view_type="electronic_invoice"}
</form>

</div>
