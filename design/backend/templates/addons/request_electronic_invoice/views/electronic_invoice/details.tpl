{if $data}
    {assign var="id" value=$data.electronic_invoice_id }
{else}
    {assign var="id" value=0}
{/if}

{assign var="allow_save" value=$request|fn_allow_save_object:"request"}

{assign var="b_type" value=$request.type|default:"G"}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit  {if !$allow_save} cm-hide-inputs{/if}" name="request_form" enctype="multipart/form-data">

{if $data}
{capture name="tabsbox"}

<input type="hidden" name="electronic_invoice_id" value="{$data.electronic_invoice_id}" />  
<input type="hidden" name="old_staff_notes" value="{$data.staff_notes}" /> 

<div id="content_general">
    <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("rei_type")}:</label>
         <div class="controls" >
         <span class="shift-input">{if $data.type == "C"}{__("company")}{/if}{if $data.type == "P"}{__("person")}{/if}</span>
         </div>
    </div>
    
    <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("rei_name")}:</label>
         <div class="controls" >
         <span class="shift-input">{$data.name}</span>
         </div>
    </div>
    
    
    <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("rei_rfc")}:</label>
         <div class="controls" >
         <span class="shift-input">{$data.rfc}</span>
         </div>
    </div>
    
    <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("address")}:</label>
         <div class="controls" >
         <span class="shift-input">{$data.address}</span>
         </div>
    </div>
    
    
    <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("state")}:</label>
         <div class="controls" >
         {if $state_descr !== ''}
         <span class="shift-input">{$state_descr}</span>
         {else}
         <span class="shift-input">{$data.state}</span>
         {/if}
         </div>
    </div>
    
    <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("city")}:</label>
         <div class="controls" >
         <span class="shift-input">{$data.city}</span>
         </div>
    </div>
    
     <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("country")}:</label>
         <div class="controls" >
         <span class="shift-input">{$data.country}</span>
         </div>
    </div>
    
     <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("rei_zip_code")}:</label>
         <div class="controls" >
         <span class="shift-input">{$data.zip_code}</span>
         </div>
    </div>
    
     <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("order_link")}:</label>
         <div class="controls" >
         <span class="shift-input"> <a href="{"orders.details?order_id=`$data.link`"|fn_url}" class="underlined">{__("order")} #{$data.link}</a></span>
         </div>
    </div>
    
    <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("payment_method")}:</label>
         <div class="controls" >
         <span class="shift-input">{$data.payment}</span>
         </div>
    </div>
    
    <div class="control-group">
        <label for="elm_data_name" class="control-label">{__("status")}:</label>
        <div class="controls" >
            <span class="shift-input">{if $data.status == "O"}{__("open")}{/if}{if $data.status == "S"}{__("sent")}{/if}</span>
        </div>
    </div>
    
    <div class="control-group">
    <label for="type_{"upload_pdf_file[`$id`]"|md5}" class="control-label {if !$attachment.upload_pdf_file}{/if}">{__("upload_pdf_file")}:</label>
    <div class="controls">
        {if $attachment.upload_pdf_file}
            <div id="box_attach_images_upload_pdf_file">
                <span class="shift-input">
                    <a href="{"electronic_invoice.get_file?attachment_id=`$attachment.upload_pdf_file.attachment_id`"|fn_url}">{$attachment.upload_pdf_file.filename}</a> ({$attachment.upload_pdf_file.filesize|formatfilesize nofilter})
                </span>
                {if $data.status == "O"}
                    <a data-ca-target-id="box_attach_images_upload_pdf_file" href="{"electronic_invoice.delete_attachment?attachment_id=`$attachment.upload_pdf_file.attachment_id`"|fn_url}" class="cm-confirm cm-post cm-ajax cm-tooltip" data-ca-event="ce.delete_attachment" title="{__("delete_pdf_file")}"><i class="icon-remove"></i></a>
                {/if}
            </div>
        {elseif $data.status == "S"}
            <span class="shift-input">
                {__('none')}
            </span>
        {/if}
        {if $data.status == "O"}
            {include file="common/fileuploader.tpl" var_name="upload_pdf_file[`$id`]"}
        {/if}
    </div>
</div>    

<div class="control-group ">
    <label for="type_{"upload_xml_file[`$id`]"|md5}" class="control-label {if !$attachment.upload_xml_file}{/if}">{__("upload_xml_file")}:</label>
    <div class="controls">
        {if $attachment.upload_xml_file}
            <div id="box_attach_images_upload_xml_file">
                <span class="shift-input">
                    <a href="{"electronic_invoice.get_file?attachment_id=`$attachment.upload_xml_file.attachment_id`"|fn_url}">{$attachment.upload_xml_file.filename}</a> ({$attachment.upload_xml_file.filesize|formatfilesize nofilter})
                </span>
                {if $data.status == "O"}
                    <a data-ca-target-id="box_attach_images_upload_xml_file" href="{"electronic_invoice.delete_attachment?attachment_id=`$attachment.upload_xml_file.attachment_id`"|fn_url}" class="cm-confirm cm-post cm-ajax cm-tooltip" data-ca-event="ce.delete_attachment" title="{__("delete_xml_file")}"><i class="icon-remove"></i></a>
                {/if}
            </div>
        {elseif $data.status == "S"}
            <span class="shift-input">
                {__('none')}
            </span>
        {/if}
        {if $data.status == "O"}
            {include file="common/fileuploader.tpl" var_name="upload_xml_file[`$id`]"}
        {/if}
    </div>
</div>    
    
<div class="control-group">
    <label for="staff_notes" class="control-label">{__("staff_notes")}:</label>
    <div class="controls" >
        <textarea name="staff_notes" id="staff_notes" maxlength="255" cols="5" rows="3" style="width:300px" {if $auth.user_type == 'V'}disabled="disabled"{/if}>{$data.staff_notes}</textarea>
        <div style="margin-left:210px" class="muted">
            {if $data.staff_notes_date}
                {$data.staff_notes_date|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
            {/if}
        </div>
    </div>
    
</div>    

</div> 

{/capture}

{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {if !$id}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="request_form" but_name="dispatch[electronic_invoice.update]"}
    {else}
        {if "ULTIMATE"|fn_allowed_for && !$allow_save}
            {assign var="hide_first_button" value=true}
            {assign var="hide_second_button" value=true}
        {/if}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[electronic_invoice.update]" but_role="submit-link" but_target_form="request_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$id}
    {/if}
{/capture}
    
</form>

{/capture}

{capture name="title"}
    {__("ei")} #{$data.electronic_invoice_id}
    <span class="f-small">
        {$data.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
    </span>
{/capture}

{include file="common/mainbox.tpl" title=$smarty.capture.title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}
<script type="text/javascript">
(function(_, $) {
    $.ceEvent('on', 'ce.delete_attachment', function(r, p) {
        if (r.deleted == true) {
            $('#' + p.result_ids).remove();
        }        
    });    
}(Tygh, Tygh.$));    
</script>