{capture name="mainbox"}

<div id="content_electronic_invoice">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

    {assign var="page_title" value=__("electronic_invoice")}
    {assign var="get_additional_statuses" value=false}
    
{assign var="order_status_descr" value=$smarty.const.STATUSES_ORDER|fn_get_simple_statuses:$get_additional_statuses:true}
{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}

{if $orders}
<table width="100%" class="table table-middle">
<thead>
<tr>
    <th width="15%"><a class="cm-ajax" href="{"`$c_url`&sort_by=id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id_of_the_request")}{if $search.sort_by == "id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=link&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("order_link")}{if $search.sort_by == "link"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("name_or_company")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="15%"><a class="cm-ajax" href="{"`$c_url`&sort_by=type&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("rei_type")}{if $search.sort_by == "type"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="15%"><a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="15%"><a class="cm-ajax" href="{"`$c_url`&sort_by=timestamp&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("date")}{if $search.sort_by == "timestamp"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
</tr>
</thead>
{foreach from=$orders item="o"}
<tr>
    <td> 
        <a href="{"electronic_invoice.details?request_id=`$o.electronic_invoice_id`"|fn_url}" class="underlined">{__("id")} #{$o.electronic_invoice_id}</a>
    </td>
    <td>
        <a href="{"orders.details?order_id=`$o.link`"|fn_url}" class="underlined">{__("order")} #{$o.link}</a>
    </td>
    <td>
        {$o.name}
    </td>
    <td>
        {if $o.type == "P"}{__("Person")}{/if}
        {if $o.type == "C"}{__("Company")}{/if}
    </td>
    <td>
        {if $o.status == "S"}{__("Sent")}{/if}
        {if $o.status == "O"}{__("Open")}{/if}
    </td>
    <td>
        {$o.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
    </td> 
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

</div>
{/capture}

{capture name="sidebar"}
    {include file="addons/request_electronic_invoice/views/electronic_invoice/components/electronic_invoice_search_form.tpl" dispatch="electronic_invoice.manage"}
{/capture}

{include file="common/mainbox.tpl" title=$page_title content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar content_id="content_electronic_invoice"}

