{if $can_rei}
    {include file="buttons/button.tpl" but_role="text" but_text=__("request_electronic_invoice") but_href="electronic_invoice.request?order_id=`$order_info.order_id`" but_meta="ty-btn__text" but_icon="ty-icon-doc-text orders-print__icon"}
{/if}