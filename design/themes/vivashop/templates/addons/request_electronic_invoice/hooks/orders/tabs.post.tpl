{if $attachments_data}
    <div class="{if $selected_section != "ei_attachments"}hidden{/if}" id="content_ei_attachments">
        {foreach from=$attachments_data item="file"}
            <p>
                {$file.filename}, 
                {$file.filesize|formatfilesize nofilter} - 
                {$file.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
                [<a target="_blank" href="{"electronic_invoice.get_file?attachment_id=`$file.attachment_id`"|fn_url}">{__("download")}</a>]
            </p>
        {/foreach}
    </div>
{/if}