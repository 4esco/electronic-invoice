{include file="views/profiles/components/profiles_scripts.tpl"}

<div class="ty-control-group">

<form action="{""|fn_url}" method="post" target="_self" name="electronic_invoice_form">

<input type="hidden" name="order_id" value={$smarty.request.order_id} />

<div class="control-group">
    <label class="control-label cm-required" for="rei_type">{__("rei_type")}:</label>
    <select class="ty-input-text-large" name="rei_data[type]" id="rei_type">
        <option value="P" {if $rei_data.type == "P"}selected="selected"{/if}>{__("rei_person")}</option>
        <option value="C" {if $rei_data.type == "C"}selected="selected"{/if}>{__("rei_company")}</option>
    </select>
</div>

<div class="control-group">
    <label for="rei_name" class="ty-control-group__title cm-required">{__("rei_name")}</label>
    <input type="text" id="rei_name" name="rei_data[name]" class="ty-input-text-large" size="50" maxlength="50" />
</div>   
 
<div class="control-group">
    <label for="rei_rfc" class="ty-control-group__title cm-required">{__("rei_rfc")}</label>
    <input type="text" id="rei_rfc" name="rei_data[rfc]" class="ty-input-text-large" size="50" maxlength="255" />
</div>   
    
<div class="control-group">
    <label for="rei_address" class="ty-control-group__title cm-required">{__("rei_address")}</label>
    <input type="text" id="rei_address" name="rei_data[address]" class="ty-input-text-large" size="50" maxlength="255" />
</div>

<div class="control-group">
<label for="rei_state" class="ty-control-group__title cm-required">{__("rei_state")}</label>
<select class="ty-input-text-large cm-state cm-location-billing" id="rei_state" name="rei_data[state]">
    <option value="">- {__("select_state")} -</option>
    {if $states && $states.$_country}
        {foreach from=$states.$_country item=state}
            <option {if $_state == $state.code}selected="selected"{/if} value="{$state.code}">{$state.state}</option>
        {/foreach}
    {/if}
</select>   
<input type="text" id="rei_state_d" name="rei_data[state]" size="32" maxlength="32" value="{$_state}" disabled="disabled" class="ty-input-text-large cm-state cm-location-billing input-large hidden cm-skip-avail-switch" />
</div>

<div class="control-group">
    <label for="rei_city" class="ty-control-group__title cm-required">{__("rei_city")}</label>
    <input type="text" id="rei_city" name="rei_data[city]" class="ty-input-text-large" size="50" maxlength="32" />
</div>
    
<div class="control-group">
<label for="rei_country" class="ty-control-group__title cm-required">{__("rei_country")}</label>
    <select id="rei_country" class="ty-input-text-large cm-country cm-location-billing" name="rei_data[country]">
        <option value="">- {__("select_country")} -</option>
        {foreach from=$countries item="country" key="code"}
            <option {if $_country == $code}selected="selected"{/if} value="{$code}">{$country}</option>
        {/foreach}
    </select>
</div>
    
<div class="control-group">
    <label for="rei_zip_code" class="ty-control-group__title cm-required">{__("rei_zip_code")}</label>
    <input type="text" id="rei_zip_code" name="rei_data[zip_code]" class="ty-input-text-large" size="50" maxlength="32" />
</div>
</div>
    
<div class="button-container">
    {include file="buttons/continue.tpl" but_name="dispatch[electronic_invoice.request]"}
</div>

</form>

{capture name="mainbox_title"}{__("request_electronic_invoice")}{/capture}